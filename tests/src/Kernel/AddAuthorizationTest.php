<?php

namespace Drupal\Tests\solcast\Kernel;

use Drupal\http_client_manager\Event\HttpClientEvents;
use Drupal\http_client_manager\Event\HttpClientHandlerStackEvent;
use Drupal\KernelTests\KernelTestBase;
use GuzzleHttp\HandlerStack;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Kernels tests for reacting upon events provided by "http_client_manager".
 *
 * @group solcast
 * @group solcast_key
 */
class AddAuthorizationTest extends KernelTestBase {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'key',
    'http_client_manager',
    'solcast',
    'solcast_key',
    'solcast_test_key',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(static::$modules);

    $this->eventDispatcher = $this->container->get('event_dispatcher');
  }

  /**
   * Test the event-subscriber for adding the authentication-key to the Request.
   */
  public function testAuthorization(): void {
    $handler = HandlerStack::create();
    $event = new HttpClientHandlerStackEvent($handler, 'solcast_services');
    $this->eventDispatcher->dispatch($event, HttpClientEvents::HANDLER_STACK);

    $this->assertStringContainsString('solcast_services', (string) $handler);
  }

}
